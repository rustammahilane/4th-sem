// an illustration of file structure system call on linux

// #include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main(void)
{
    char txt[] = "This is a text with 36 characters.\n";
    
    if(write(1, txt, 36) != 35)
        write(2, "A write error has occured at file descriptor 1.\n", 49);
    exit(0);
}