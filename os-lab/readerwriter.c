// reader writer
#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

sem_t sem;
pthread_mutex_t mutex;
int readerNum = 0;
int value = 5;

void* Read(void* rNum)
{
    printf("Reader %d is trying to read...\n", *(int*)rNum);
    pthread_mutex_lock(&mutex);
    readerNum++;
    if(readerNum == 1) {
        sem_wait(&sem);
    }
    pthread_mutex_unlock(&mutex);

    printf("Reader %d read the values as : %d\n", *(int*)rNum, value);
    sleep(0);
    pthread_mutex_lock(&mutex);
    readerNum--;
    if(readerNum == 0) {
        sem_post(&sem);
    }
    pthread_mutex_unlock(&mutex);
}

void* Write(void* wNum)
{
    printf("Writer %d is trying to write...\n", *(int*)wNum);
    sem_wait(&sem);
    value = value + 1;
    printf("Writer %d modified value to : %d\n", *(int*)wNum, value);
    sem_post(&sem);
}

int main()
{
    pthread_t readers[10], writers[5];
    pthread_mutex_init(&mutex, NULL);
    sem_init(&sem, 0, 1);

    int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int i;
    for(i = 0; i < (sizeof(readers) / sizeof(readers[0])); i++) {
        pthread_create(&readers[i], NULL, Read, &numbers[i]);
    }

    for(i = 0; i < (sizeof(writers) / sizeof(writers[0])); i++) {
        pthread_create(&writers[i], NULL, Write, &numbers[i]);
    }

    for(i = 0; i < (sizeof(readers) / sizeof(readers[0])); i++) {
        pthread_join(readers[i], NULL);
    }

    for(i = 0; i < (sizeof(writers) / sizeof(writers[0])); i++) {
        pthread_join(writers[i], NULL);
    }

    pthread_mutex_destroy(&mutex);
    sem_destroy(&sem);

    return 0;
}
