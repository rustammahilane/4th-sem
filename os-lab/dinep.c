#include<stdio.h>
#include<unistd.h>
#include<semaphore.h>
#include<pthread.h>

int chopsticks[5]={1,1,1,1,1};
int m=1;

void eating(int i)
{
   if(chopsticks[(i-1)%5]==1 && chopsticks[i%5]==1)
  {
     chopsticks[i%5]=0;
     chopsticks[(i-1)%5]=0;
     printf("Philosopher %d eats\n",i);
  }
   else
  {
     printf("Chopstick not available\n");
  }
}

void thinking(int i)
{
   chopsticks[i%5]=1;
   chopsticks[(i-1)%5]=1;
   printf("philosopher %d thinks\n",i);
}

int main()
{
   int t;
   printf("Enter value for process to be conducted :\n 1)Think\n 2)Eat \n 3)Terminate\n");

   for(int i=0;;i++)
   {
      printf("Enter value for next process:\n");
      scanf("%d",&t);
      if(t==1 && m==1)
      {
         int x;
         printf("Enter the philosopher who wants to think:\n");
         scanf("%d",&x);
         thinking(x);
      }
      else if(t==2 && m==1)
      {
         int x;
         printf("Enter the philosopher who wants to eat:\n");
         scanf("%d",&x);
         eating(x);
      }
      else if(t==3)
      {
         break;
      }
   }
  return 0;
}

