// first come first serve algortihm

#include <stdio.h>

int calculateWaitingTime(int at[], int bt[], int N){
    int wt[N];
    wt[0] = 0;
	int ct[N], tat[N];
	ct[0] = bt[0]; tat[0] = bt[0];
    float average= 0;
    float sum = 0;
	float t_sum = 0;

    printf("pID\t\tAT\t\tBT\t\tCT\t\tTAT\t\tWT\n");
printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n", 1, at[0], bt[0], ct[0], tat[0], wt[0]);
    for(int i = 1; i < N; i++){
        wt[i] = at[i-1] + bt[i-1] + wt[i-1] - at[i];
		sum += wt[i];
		ct[i] += bt[i];
		tat[i] = ct[i] - at[i];
		t_sum += tat[i];
        printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n", i+1, at[i], bt[i], ct[i], tat[i], wt[i]);
    }

    average = sum / N ;
    printf("Average waiting time = %f\n", average);
    printf("Average turn around time = %f\n", t_sum / N);

}

int main(void)
{
	printf("First Come First Serve\n");
    // same arrival time of all process
    int at[] = { 0, 0,  0 };

    // burst times 
    int bt[] = {25,10,2 };

    calculateWaitingTime(at, bt, 3);

    return 0;
}
