// shortest job first - cpu scheduling

#include <stdio.h>

void insertion_sort(int n, int A[n]){
     for(int i = 1; i < n; i++){
        int next = A[i], j;
        for(j = i - 1; j >= 0 && A[j] > next; j--)
            A[j+1] = A[j];
        A[j+1] = next;
    }
}

int calculateWaitingTime(int at[], int bt[], int N){
    int index[N];
	int wt[N], tat[N], ct[N];
    for(int i = 0; i < N; i++)
        index[i] = bt[i];

    insertion_sort(N, index);

    float sum = 0;
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if ( index[i] == bt[j]){
	  	    wt[j] = sum ;
		    sum += bt[j];
		    ct[j] = sum;
		    tat[j] = ct[j] - at[j];
		}
        }
    }
    	sum = 0;
	float t_sum = 0;
	printf("pID\t\tAT\t\tBT\t\tCT\t\tTAT\t\tWT\n");
	for(int i = 0; i < N; i++){
		printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n",i+1, at[i], bt[i], ct[i], tat[i], wt[i]);
		sum += wt[i];
		t_sum += tat[i];
	}

	printf("Average waiting time : %f\n", sum / N);
	printf("Average turn around time : %f", t_sum / N );

}
int main(void)
{

	printf("Shortest Job First\n");
    int at[] = { 0, 0, 0};
    int bt[] = { 25, 10, 2};
	calculateWaitingTime(at, bt, 3);
}
