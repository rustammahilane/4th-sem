#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>

struct msg_buffer {
	long msg_type;
	char msg_txt[100];
} message;

int main()
{
	key_t key;
	int msg_id;
	
	key = ftok("progfile", 65);

	msg_id = msgget(key, 0666 | IPC_CREAT);
	message.msg_type = 1;

	printf("writing data: ");
	fgets(message.msg_txt, 20, stdin);

	msgsnd(msg_id, &message, sizeof(message), 0);
	printf("data send is : %s n", message.msg_txt);
	
	return 0;
}
