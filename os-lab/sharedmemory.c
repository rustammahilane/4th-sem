// writing to shared memory
#include <sys/types.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
int main()
{
	int i;
	void *sm;
	char buff[100];
	int shmid;
	shmid = shmget((key_t)1001, 1024, 0666 | IPC_CREAT);
	printf("key of shared memory is %d\n", shmid);

	sm = shmat(shmid, NULL, 0);
	printf("process attached at %p\n",sm);
	printf("enter some data to write to shared memory\n");

	read(0, buff, 100);
	strcpy(sm, buff);
	printf("you wrote: %s\n", (char*)sm);

}
